@extends('layouts.app')

@section('title', 'Marketplace')

@section('content')

    <form action="{{route('products.update', ['id' => $product->id])}}" method="post">

        {{ method_field('PUT') }}
        {{csrf_field()}}

        <input class="form-control" type="text" name="name" placeholder="Name"
               value="{{ old('name')?:$product->name }}">
        @if($errors->has('name'))
            <div class="alert alert-danger">{{$errors->first('name')}}</div>
        @endif

        <input class="form-control" type="text" name="price" placeholder="Price"
               value="{{ old('price')?:$product->price }}">
        @if($errors->has('price'))
            <div class="alert alert-danger">{{$errors->first('price')}}</div>
        @endif

        <button style="font-weight: 600;" class="btn btn-success" type="submit">Save</button>

    </form>

@endsection
