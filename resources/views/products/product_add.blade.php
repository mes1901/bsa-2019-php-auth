@extends('layouts.app')

@section('title', 'Marketplace')

@section('content')

    <h2>MarketPlace</h2>
    <form action="{{route('products.store')}}" method="post">

        {{csrf_field()}}

        <input class="form-control" type="text" name="name" placeholder="Name" value="{{ old('name') }}">
        @if($errors->has('name'))
            <div class="alert alert-danger">{{$errors->first('name')}}</div>
        @endif

        <input class="form-control" type="text" name="price" placeholder="Price" value="{{ old('price') }}">
        @if($errors->has('price'))
            <div class="alert alert-danger">{{$errors->first('price')}}</div>
        @endif

        <button style="font-weight: 600;" class="btn btn-success" type="submit">Save</button>

    </form>

@endsection
