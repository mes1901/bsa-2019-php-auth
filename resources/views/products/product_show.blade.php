@extends('layouts.app')

@section('title', 'Marketplace')

@section('content')

    <h3>{{$product->name}}</h3>
    @if (Auth::user()->can('update', $product))
        <a style="font-weight: 300;" class="edit-button btn btn-info"
           href="{{route('products.edit', ['id' => $product->id])}}">Edit</a>

        <form style="display: inline-block;" action="{{ route('products.destroy', ['id' => $product->id]) }}"
              method="POST">

            {{ csrf_field() }}
            {{ method_field('DELETE') }}

            <button style="font-weight: 300;" class="delete-button btn btn-danger" type="submit">Delete
            </button>
        </form>
    @endif

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Title</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Name</th>
                <td>
                    <h4>{{$product->name}}</h4>
                </td>
            </tr>
            <tr>
                <th>Price</th>
                <td>
                    <h4>{{$product->price}}</h4>
                </td>
            </tr>
        </tbody>
    </table>

@endsection
