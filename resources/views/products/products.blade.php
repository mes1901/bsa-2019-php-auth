@extends('layouts.app')

@section('title', 'Marketplace')

@section('content')

    <h2>MarketPlace</h2>
    @if($products->count() > 0)
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
                <tr>
                    <td>
                        <h4><a href="{{route('products.show', ['id' => $product->id])}}">{{ $product->name}}</a>
                        </h4>
                    </td>
                    <td>
                        <h4>{{ $product->price }}</h4>
                    </td>
                    @if (Auth::user()->can('update', $product))
                            <td>
                                <a style="font-weight: 600;" class="btn btn-info"
                                   href="{{route('products.edit', ['id' => $product->id])}}">Edit</a>
                                <form style="display: inline-block;"
                                      action="{{ route('products.destroy', ['id' => $product->id]) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button style="font-weight: 300;" class="btn btn-danger" type="submit">Delete
                                    </button>
                                </form>
                            </td>
                     @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p>No products</p>
    @endif

@endsection
