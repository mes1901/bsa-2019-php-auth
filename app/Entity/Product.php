<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = ['name','price','user_id'];

    public function user(){
        return $this->belongsTo(User::class);
    }


}
