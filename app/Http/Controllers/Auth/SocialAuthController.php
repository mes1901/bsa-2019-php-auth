<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 17.07.2019
 * Time: 11:13
 */

namespace app\Http\Controllers\Auth;


use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    protected $redirectPath = '/products';

    public function redirectToProvider(Request $request) {
        return Socialite::driver('google')->redirect();
    }
    public function handleProviderCallback()
    {
        $socialUser = Socialite::driver('google')->user();
        $user = User::where(['email' => $socialUser->email])->first();
        // Create if not found
        if (is_null($user)) {
            $user = User::create([
                'name' => $socialUser->user['name'],
                'email' => $socialUser->email,
                'password' => 'password'
            ]);
        }
        Auth::login($user);
        return redirect($this->redirectPath);
    }

}