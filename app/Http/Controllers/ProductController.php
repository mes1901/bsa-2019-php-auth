<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $products = Product::all();
        return view('products/products', ['products' => $products]);
    }

    public function add()
    {
        if (Gate::denies('create')) {
            return redirect('/products');
        }
        return view('products/product_add');
    }

    public function store(Request $request)
    {
        if (Gate::denies('create')) {
            return redirect('/products');
        }
        $product = Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'user_id' => Auth::id()
        ]);
        $product->save();
        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('products/product_show', ['product' => $product]);
    }

    public function edit($id)
    {
        $product = Product::where('id', $id)->first();
        if (Gate::denies('update', $product)) {
            return redirect('/products');
        }
        return view('products/product_edit', ['product' => $product]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        if (Gate::denies('update', $product)) {

            return redirect('/products');
        }
        $product->update([
            'name' => $request->name,
            'price' => $request->price
        ]);
        $product->save();
        return redirect()->route('products.show', ['id' => $product->id]);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if (Gate::denies('delete', $product)) {
            return redirect('/products');
        }

        $product->delete();
        return redirect()->route('products.index');
    }
}
