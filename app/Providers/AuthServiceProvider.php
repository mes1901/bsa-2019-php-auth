<?php

namespace App\Providers;

use App\Entity\Product;
use App\Entity\User;
use Illuminate\Support\Facades\Gate;
use App\Policies\ProductsPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        Product::class => ProductsPolicy::class
    ];

    public function boot()
    {
        $this->registerPolicies();
        Gate::define('admin', function (User $user) {
            return $user->getAttribute('is_admin');
        });
        Gate::define('create', 'App\Policies\ProductsPolicy@create');
        Gate::define('edit', 'App\Policies\ProductsPolicy@update');
        Gate::define('update', 'App\Policies\ProductsPolicy@update');
        Gate::define('delete', 'App\Policies\ProductsPolicy@delete');
    }
}
