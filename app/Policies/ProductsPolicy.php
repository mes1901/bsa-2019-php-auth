<?php

namespace App\Policies;

use App\Entity\Product;
use App\Entity\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductsPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        if ($user->is_admin) {
            return true;
        }
        return $user->id;
    }

    public function update(User $user, Product $product)
    {
        if ($user->is_admin) {
            return true;
        }
        return $product->user_id === $user->id;
    }

    public function delete(User $user, Product $product)
    {
        if ($user->is_admin) {
            return true;
        }
        return $product->user_id === $user->id;
    }
}
